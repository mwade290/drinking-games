package com.mwade290.drinking.games;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class InstructionsActivity extends Activity {

	private String mGameName;
	private Games mGames = new Games();
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);
        mGameName = (String) getIntent().getExtras().get("TITLE");
        TextView gameTitle = (TextView) findViewById(R.id.title);
        TextView gameInstructions = (TextView) findViewById(R.id.gameInstructions);
        gameTitle.setText(mGameName);
        
        
        for (int i = 0; i < mGames.mGames.length; i++) {
        	if (mGameName.equals(mGames.mGames[i])) {
        		gameInstructions.setText(mGames.getGameInfo(mGameName));
        	}
        }
    }
    
}
