package com.mwade290.drinking.games;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.mwade290.drinking.games.ShakeDetector.OnShakeListener;

public class MainActivity extends Activity {
	
	private Games mGames = new Games();
	private Button mButtonGames;
	private Button mButton;
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private ShakeDetector mShakeDetector;
	private String mGameName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Game Button
		mButtonGames = (Button) findViewById(R.id.btnGame);
		mButtonGames.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startInstructionsActivity(v);
			}
		});
		
		// Instruction Button
		mButton = (Button) findViewById(R.id.button1);
		mButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				handleNewAnswer();
			}
		});
		
		// Shake Detector
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mShakeDetector = new ShakeDetector(new OnShakeListener() {
			
			@Override
			public void onShake() {
				handleNewAnswer();
			}
		});
		
		// Ensure button text is not empty
		handleNewAnswer();
	}
	
    private void startInstructionsActivity(View v) {
        Intent intent = new Intent(this, InstructionsActivity.class);
        intent.putExtra("TITLE", mGameName);
        startActivity(intent);
    }
	
	private void handleNewAnswer() {
		mGameName = (String) mButtonGames.getText();
		if (mGameName.equals("")) {
			mGameName = mGames.getGame();
		}
		while (mGameName == mButtonGames.getText()) {
			mGameName = mGames.getGame();	
		}
		mButtonGames.setText(mGameName);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		mSensorManager.registerListener(mShakeDetector, mAccelerometer, 
				SensorManager.SENSOR_DELAY_UI);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		mSensorManager.unregisterListener(mShakeDetector);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
