package com.mwade290.drinking.games;

import java.util.Random;

public class Games {
	
	public String[] mGames = {
		"Ibble Dibble",
		"Bunnies",
		"Taps",
		"Thumb Master",
		"Head Master",
		"Kings Cup",
		"Beeramid",
		"Circle of Death",
		"Give or Take",
		"Higher or Lower",
		"Pyramid",
		"Suits",
		"Landmine",
		"Three Man",
		"Magic Dice",
		"Threshold",
		"Speed Dice",
		"Forfeit Dice",
		"7-11 Doubles"
	};
	
	public String getGame() {
		
		String game = "";
		int randomNumber = 0;
//		int rN = getRandom(0,3);
//		if (rN == 1) {
//			// Pages in book
//			randomNumber = getRandom(14, 271);
//			return "Page " + randomNumber;
//		}
		randomNumber = getRandom(0, mGames.length - 1);
		game = mGames[randomNumber];
		return game;
	}
	
	public String getGameInfo(String game) {
		String gameInfo = "";
		if (game.equals("Ibble Dibble")) {
			gameInfo = "Assign everyone a number at random. \n\n" +
	    			"To start the first person must say: \n\n" +
	    			"I'm ibble dibble number ONE, I have NO dibble ibbles, " +
	    			"how many dibble ibbles does ibble dibble number TWO have?\n\n" +
	    			"Ibble Dibble number TWO will then say: \n\n" +
	    			"I'm ibble dibble number TWO, I have NO dibble ibbles, " +
	    			"how many dibble ibbles does ibble dibble number TWELVE have?\n\n" +
	    			"An ibble dibble is a person and dibble ibbles are the amount of punishment \nspots you have. \n\n" +
	    			"You have to say the sentence EXACTLY correct otherwise you get punished. \n\n" +
	    			"The punishment is burning a cork then putting a black spot on their face.";
		}
		if (game.equals("Bunnies")) {
			gameInfo = "To start, pick a Centre bunny who puts both hands next to head. \n\n" +
	    			"The person to their LEFT puts \ntheir RIGHT hand up. \n\n" +
	    			"The person to their RIGHT puts \ntheir LEFT hand up. \n\n" +
	    			"The Centre Bunny points and shouts the next person's name to pass \n(anybody they wish).\n\n" +
	    			"The original three bunnies then put their hands down (unless next to \nnew Centre Bunny). \n\n" +
	    			"Game continues with the \nnext three bunnies.\n\n" +
	    			"Any mistakes and that person must \ndrink 2 fingers of alcohol then \nbecomes Centre Bunny.";
		}
		if (game.equals("Taps")) {
			gameInfo = "Start by standing up and shouting your name to everyone (everybody cheers).\n" +
					"Next shout 'Taps' (more cheers).\n\n" +
					"The aim is to count to 21 by passing the number to the player beside you \n(either direction).\n\n" +
					"Tapping your RIGHT arm on your LEFT shoulder sends it LEFT of you.\n" +
					"Tapping your LEFT arm on your RIGHT shoulder sends it RIGHT of you.\n\n" +
					"On 7 you do not tap, you hover one hand over the other, the direction is dictated by the bottom hand.\n" +
					"On 10 you have to throw both arms in the air and shout 'Goon Time', direction continues the way it was going.\n" +
					"On 14 you do the same as 7 except the top hand dictates the direction.\n" +
					"On 21 the person who gets it has to finish their drink!\n" +
					"If a player makes a mistake, they drink 2 fingers of alcohol and start again.";
		}
		if (game.equals("Thumb Master")) {
			gameInfo = "Pick a starting Thumb Master\n\n" +
					"At any point the Thumb Master can place his/her thumb on the table.\n\n" +
					"The last person to put their \nthumb on the table loses.\n\n" +
					"The loser has to drink 2 fingers of alcohol and becomes the new Thumb Master.";
		}
		if (game.equals("Head Master")) {
			gameInfo = "Pick a starting Head Master\n\n" +
					"At any point the Head Master can place his/her head on the table.\n\n" +
					"The last person to put their \nhead on the table loses.\n\n" +
					"The loser has to drink 2 fingers of alcohol and becomes the new Head Master.\n\n" +
					"You will end up with a very sore head the closer to the last one you are!";
		}
		if (game.equals("Kings Cup")) {
			gameInfo = "Put all cards in a circle around a large glass.\n\n" +
					"Take turns to turn over a card and obey the following rules: \n\n" +
					"Ace: Race! Pick a person, \nrace to finish your drink \n" +
					"2: Fuck you! Pick a person to drink 2 fingers \n" +
					"3: Fuck me! Drink 3 fingers \n" +
					"4:	Snake eyes! If you make eye contact with another person, they drink \n" +
					"5: Waterfall! \n" +
					"6: Dicks drink\n" +
					"7: Drink for 7 seconds \n" +
					"8: Pick a mate \n" +
					"9: Make a rule \n" +
					"10: All drink \n" +
					"J: Back! Switch direction \n" +
					"Q: Question master \n" +
					"K: Pour some drink into kings cup, person to get the last King must drink the Kings cup \n";
	
		}
		if (game.equals("Beeramid")) {
			//Page 16
			gameInfo = "Lay out the cards face down in a 5,4,3,2,1 pyramid formation.\n\n" +
			"Give each player 5 cards face down.\n\n" +
			"Start at the top of the pyramid and flip the first card over.\n\n" +
			"If anyone has the same number in their hand as the flipped card, they can nominate someone to drink.\n\n" +
			"On step one of the pyramid you have to drink one finger if nominated, step two, two fingers and so on...";
		}
		if (game.equals("Circle of Death")) {
			//Page 17
			gameInfo = "Lay out all cards face down \nin a circle formation\n\n" +
			"The starting person is to flip one card over and reveal it to everyone\n\n" +
			"The player to his left is to flip another card and reveal it to everyone\n\n" +
			"If the cards are the same suit then both players have to drink the \ntotal sum of both cards\n\n" +
			"E.g: If 4 of clubs and 8 of clubs, \ndrink for 12 seconds\n\n" +
			"Jack, Queen and King = 10 and Ace is 11\n\n" +
			"If the suits don't match then discard the first card and continue left";
		}
		if (game.equals("Give or Take")) {
			//Page 21
			gameInfo = "Lay the cards face down in a 6x6 formation.\n\n" +
			"Deal the rest of the cards out between the players.\n\n" +
			"The first, third and fifth rows are 'GIVE'.\n\n" +
			"The second, forth and sixth rows are 'TAKE'.\n\n" +
			"The first row is two seconds of drinking, second is four seconds and so on...\n\n" +
			"Flip the top left card over and if anyone has a card/cards with the same suit, " +
			"they get to 'GIVE another player the drinking penalty.\n\n" +
			"Continue along the top row in this manner.\n\n" +
			"When moving onto the second row you have to 'TAKE' the drinking penalty instead.\n\n" +
			"Don't forget the different number of drinking seconds on every row!";
		}
		if (game.equals("Higher or Lower")) {
			//Page 24
			gameInfo = "Decide on a Player and a Dealer.\n\n" +
			"The Dealer then turns over a card (remember the value of this card), the Player then has to guess " +
			"whether the next card is higher or lower than the current card.\n\n" +
			"If the Player guesses CORRECTLY, the Dealer has to drink two fingers of alcohol.\n\n" +
			"If the Player guesses INCORRECTLY, the Player has to drink two fingers of alcohol.\n\n" +
			"Continue until a card with the same value as the first is revealed.";
		}
		if (game.equals("Pyramid")) {
			//Page 25
			gameInfo = "Lay out the cards face down in a \n5,4,3,2,1 pyramid formation.\n\n" +
			"The chosen player must make his way up the pyramid from bottom to top.\n\n" +
			"If a Jack, Queen, King or Ace are revealed then the player must drink 2 fingers of alcohol " +
			"and all flipped cards must be replaced by fresh face down cards.\n\n" +
			"Continue until the pyramid is beaten.";
		}
		if (game.equals("Suits")) {
			//Page 29
			gameInfo = "Nominate a Dealer who has to decide on a suit and tell everyone.\n\n" +
			"The Dealer then starts dealing a card face up to each player.\n\n" +
			"If the suit on the player's card matches the suit the Dealer called then " +
			"the player must drink for how many seconds shown \nas the card value.\n\n" +
			"E.g: 8 Diamonds = 8 Seconds.\n\n" +
			"The person to the right of the drinker is to count down the seconds.\n\n" +
			"The player then has to decide a new suit and play continues.";
		}
		if (game.equals("Landmine")) {
			//Page 36
			gameInfo = "Lay out 6 cards face down in a row.\n\n" +
			"The chosen player must make his way across the minefield from left to right.\n\n" +
			"Numbers 2-10 are safe and play continues to the next card.\n\n" +
			"If any other cards are flipped, more cards are added to the minefield and the player must " +
			"drink 1 finger of alcohol for every added card.\n\n" +
			"Jack: 1 Card\n" +
			"Queen: 2 Cards\n" +
			"King: 3 Cards\n" +
			"Ace: 4 Cards\n\n" +
			"Continue until the player completes the minefield";
		}
		if (game.equals("Three Man")) {
			//Page 88
			gameInfo = "One by one, each player must roll 2 dice until one of the dice reads '3', " + 
			"this person is now the 'Three Man'\n\n" +
			"Each player rolls the two dice and follows the guidelines below: \n\n" +
			"Double: Everyone but the dice roller drinks 2 fingers of alcohol\n" +
			"Total of 3: The Three Man must drink 4 fingers of alcohol (The player to his left is now the 'Three Man')\n" +
			"4, 5, 6: The Person to the dice throwers LEFT must drink for how many seconds shown as the dice value\n" +
			"8, 9, 10: The Person to the dice throwers RIGHT must drink for how many seconds shown as the dice value\n" +
			"7: Nominate someone to drink for 7 seconds\n" +
			"11: Dice thrower drinks for 11 seconds";
		}
		if (game.equals("Magic Dice")) {
			//Page 93
			gameInfo = "Nominate the 'Mouth' player then each player take one dice each.\n\n" +
			"The 'Mouth' calls out a number between 1 and 6 as each player rolls their dice at once " +
			"(keep an eye on your dice).\n\n" +
			"If your dice has the same value as the nominated number you must drink \n2 fingers of alcohol.\n\n" +
			"Alternatively the 'Mouth' can call out \nODD or EVEN.\n\n" +
			"You must drink 3 fingers of alcohol if the 'Mouth' guessed correctly but if 1,3 and 5 are ALL shown " +
			"between all the dice, the 'Mouth' must drink 4 fingers of alcohol and the 'Mouth' becomes " +
			"the player to his left.";
		}
		if (game.equals("Threshold")) {
			//Page 94
			gameInfo = "Get a cup, place 2 dice and a coin inside it.\n\n" +
			"The Player is to guess \nheads/tails before the throw.\n\n" +
			"When the Shaker throws the dice, if the Player guessed INCORRECTLY, " + 
			"the Player must drink the number of seconds of alcohol as there are spots on the dice.\n\n" +
			"If the Player gussed CORRECTLY, then the Shaker must drink instead.\n\n" +
			"Try with 3 dice...";
		}
		if (game.equals("Speed Dice")) {
			//Page 104
			gameInfo = "Each player rolls 2 dice.\n\n" +
			"The loser is the player with the lowest score and has to drink the number of seconds " +
			"of the difference between the \nhighest and lowest score.\n\n" +
			"E.g: Highest = 10, Lowest = 3... \nDrink for 7 seconds.";
		}
		if (game.equals("Forfeit Dice")) {
			//Page 105
			gameInfo = "The Roller has to roll 2 dice to get a base number.\n\n" +
			"Then the Player must guess higher/lower than this number and the Roller rolls again.\n\n" +
			"Each Player gets 3 goes like this, then play moves on to the next Player.\n\n" +
			"If they are wrong TWICE back-to-back however, they must roll the forfeit dice...\n\n" +
			"The Roller must roll 2 more dice and the Player is to drink for the number of seconds " +
			"as there are spots on the dice.";
		}
		if (game.equals("7-11 Doubles")) {
			//Page 106
			gameInfo = "The Roller is to roll 2 dice, if the dice land 7,11 or any double " +
			"then the Drinker to his right must pick up his drink.\n\n" +
			"At the same time, the Drinker has to drink non-stop until the Roller lands another 7, 11 or double.\n\n" +
			"Play is passed to the person on the Roller's left when complete.";
		}
		return gameInfo;
	}
	
	private int getRandom(int min, int max) {
		
		int randomNum = 0;
		Random randomGenerator = new Random();
		randomNum = randomGenerator.nextInt(((max + 1) - min)) + min;
		return randomNum;
	}

}
